Instrukcja instalacji oraz uruchmienia aplikacji zestawiajacej polaczenia

1. Pobranie 

    -uruchom git bash
    -wpisz polecenie git clone https://zalwertrafal@bitbucket.org/zalwertrafal/project1_srodowisko.git
    

2. Instalacja pakietów
    
    Wymagania:
        Visual Studio Code 
        NodeJS

    Otwórz pobrany wcześniej folder za pomocą Visual Studio Code.Następnie w terminalu(Ctrl + Shift + `)
    wpisz polecenie "npm install".

3. Konfiguracja

    Znajdz plik "dialer.js". W lini 13 znajdziesz pola "login" i "password". Wpisz tam swoje dane logowania do serwera.

4. Uruchamianie
    
    We wczesniej otwartym terminalu wpisz polecenie "node dialer.js".Otrzymasz komunikat "app listening on port 3000" to znaczy ze serwer wystartowal.

    Nastepnie uruchom swoja przeglądarke w polu na adres wpisz "localhost:3000/call/<numer1>/<numer2>. Po chwili powinno zostac zestawione połączenie.

