const express = require('express');
const app = express();
const Dialer = require('dialer').Dialer;
const cors = require('cors');
const bodyParser = require('body-parser');
const http = require('http').Server(app);
const io = require('socket.io')(http);

let bridge = null;
let status = null;
let statusold = null;
let interval = null;
const config = {
    url: 'https://uni-call.fcc-online.pl',
    login: '*******',
    password: '********'
};

Dialer.configure(config);
app.use(cors());
app.use(bodyParser.json());
http.listen(3000, () => {
    console.log('app listening on port 3000');
});

io.on('connection' , (socket) =>{
    console.log('a user connected')
    socket.on('disconnet' , () =>{
        console.log('a user diconnected')
    })
    socket.on('message',(message)=>{
        console.log('message' , message)
    socket.on('status',(status) =>{
        console.log('status', status)
    })
    })
    io.emit('message','connected!')
})

app.get('/call/:number1/:number2', async (req, res) => {
    const number1 = req.params.number1;
    const number2 = req.params.number2;
    Dialer.call(number1, number2);
    bridge = await Dialer.call(number1,number2);
    res.json({ success: true });
})

app.get('/status', async (req,res)=>{
    

    if(bridge != null){
        status = await bridge.getStatus();
    }
    res.json({success:true, status: status});
} );

app.post('/call/', async (req, res) => {
    const body = req.body;
    console.log(body);

    const number1 = body.number1;
    const number2 = body.number2;
    bridge = await Dialer.call(number1, number2);
     interval = setInterval(async() => {
        status = await bridge.getStatus();
        
        console.log(status)
        if(status !== statusold){
            statusold = status;
           io.emit('status', status); 
        }
        
    },500)
    res.json({ success: true, status: status });
    })
